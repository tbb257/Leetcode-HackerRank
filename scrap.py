import requests

API_Key = "AIzaSyAzFP86sGdwOEOeKowQODGFj9Lj5A1hhJE"


"Basic Volume Search"
prompts = {":":"%3A", " ":"%20"}

URL1= f"https://www.googleapis.com/books/v1/volumes?q=flowers+inauthor:keyes&key={API_Key}"
URL2= f"https://www.googleapis.com/books/v1/volumes?q=subject{prompts[':']}Juvenile{prompts[' ']}Fiction"

res = requests.get(URL2)
results = res.json()

output = []
for items in results["items"]:
    details = items["volumeInfo"]
    object = {}

    object["Title"] = details["title"]
    object["Author"] = details["authors"]
    object["Published Date"] = details["publishedDate"]
    object["Thumbnail"] = details["imageLinks"]["thumbnail"]
    object["Google Link"] = details["canonicalVolumeLink"]
    object["Preview"] = details["previewLink"]
    output.append(object)


print(output)
