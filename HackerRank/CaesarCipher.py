"""
Link = https://www.hackerrank.com/challenges/one-month-preparation-kit-angry-children/problem?h_l=interview&isFullScreen=true&playlist_slugs%5B%5D%5B%5D=preparation-kits&playlist_slugs%5B%5D%5B%5D=one-month-preparation-kit&playlist_slugs%5B%5D%5B%5D=one-month-week-two

"""


def caesarCipher(s, k):
    if k > 26:
        k = k % 26
    encoder = "abcdefghijklmnopqrstuvwxyz"
    caps_encoder = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    cipher = ""

    for i in range(len(s)):
        if s[i] not in encoder and s[i] not in caps_encoder:
            cipher += s[i]
        elif s[i] in encoder:
            index = encoder.index(s[i])
            offset = index + k
            if offset > 25:
                offset -= 26
            cipher += encoder[offset]
        else:
            index = caps_encoder.index(s[i])
            offset = index + k
            if offset > 25:
                offset -= 26
            cipher += caps_encoder[offset]
    return cipher



string = "There's-a-waiting-in-the-sky"
k = 3

print(caesarCipher(string, k))
