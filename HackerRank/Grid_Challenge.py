"""
Link = https://www.hackerrank.com/challenges/one-month-preparation-kit-grid-challenge/problem?h_l=interview&isFullScreen=true&playlist_slugs%5B%5D%5B%5D=preparation-kits&playlist_slugs%5B%5D%5B%5D=one-month-preparation-kit&playlist_slugs%5B%5D%5B%5D=one-month-week-two

"""


def gridChallenge(grid):
    sorted_row=list(map(lambda x:sorted(x),grid))
    transposed_array = list(map(list, zip(*sorted_row)))
    for row in transposed_array:
        if row!=sorted(row):
            return "NO"
    return "YES"



grid = ["abc","ade","efg"]
print(gridChallenge(grid))
