"""
Link = https://www.hackerrank.com/challenges/one-month-preparation-kit-angry-children/problem?h_l=interview&isFullScreen=true&playlist_slugs%5B%5D%5B%5D=preparation-kits&playlist_slugs%5B%5D%5B%5D=one-month-preparation-kit&playlist_slugs%5B%5D%5B%5D=one-month-week-two

"""


def maxMin(k, arr):
    arr.sort()
    unfairness = float("inf")
    for i in range(len(arr)-k+1):
        window = arr[i:i+k]
        # - Taking too much time with the max/min methods. Just do arr[upper range] - arr[low range]
        unfairness = min(unfairness, (max(window)-min(window)))

        # unfairness = min(unfairness, arr[i+k-1]-arr[i])
    return unfairness



test = [100,200,300,350,400,401,402]
k = 3

print(maxMin(k, test))
