"""
Link: https://leetcode.com/problems/generate-parentheses/

Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.

Logic:
Evalute conditions where you can add ( or ).
If count of ( is less than n, you can add (. If count of ) is less than count of (, you can add ).
Therefore, you must keep count of both ( and ), as well as length of entire string.

Use recursion to account for both cases, to encompass all combinations of ().
"""

def generateParenthesis(n):
    """
    :type n: int
    :rtype: List[str]
    """
    result = []
    string = ""

    def dfs(left, right, string):
        if left == right == n:
            #Could also write if left+right == 2*n or length(string) == 2*n
            result.append(string)
            return
        if left < n:
            dfs(left+1, right, string + "(")
        if right < left:
            dfs(left, right+1, string + ")")

    dfs(0, 0, string)

    return result

test = 3
print(generateParenthesis(test))
