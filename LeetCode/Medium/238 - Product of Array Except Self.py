"""
Link: https://leetcode.com/problems/product-of-array-except-self/
Given an integer array nums, return an array answer such that answer[i] is equal to the product of all the elements of nums except nums[i].

The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit integer.

You must write an algorithm that runs in O(n) time and without using the division operation.


"""

def productExceptSelf(nums):
    """
    :type nums: List[int]
    :rtype: List[int]
    """
    res = [1] * (len(nums))

    prefix = 1
    for i in range(len(nums)): # from left to right
        res[i] = prefix
        prefix *= nums[i]

    postfix = 1
    for i in range(len(nums)-1,-1,-1): # from right to left
        res[i] *= postfix
        postfix *= nums[i]

    return res

nums = [1,2,3,4]


print(productExceptSelf(nums))
