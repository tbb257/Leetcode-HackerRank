"""
Link: https://leetcode.com/problems/container-with-most-water/

You are given an integer array height of length n. There are n vertical lines drawn such that the two endpoints of the ith line are (i, 0) and (i, height[i]).

Find two lines that together with the x-axis form a container, such that the container contains the most water.

Return the maximum amount of water a container can store.

Notice that you may not slant the container.
"""
def maxArea(height):
    maximum = 0
    left = 0
    right = len(height)-1
    while left < right:
        h1 = height[left]
        h2 = height[right]
        area = (right-left) * min(h1, h2)
        if area > maximum:
            maximum = area
        if h1 <= h2:
            left += 1
        else:
            right -= 1
    return maximum

test = [1,8,6,2,5,4,8,3,7]
print(maxArea(test))
