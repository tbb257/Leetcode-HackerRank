"""
Link: https://leetcode.com/problems/number-of-wonderful-substrings/

A wonderful string is a string where at most one letter appears an odd number of times.

For example, "ccjjc" and "abab" are wonderful, but "ab" is not.
Given a string word that consists of the first ten lowercase English letters ('a' through 'j'), return the number of wonderful non-empty substrings in word. If the same substring appears multiple times in word, then count each occurrence separately.

A substring is a contiguous sequence of characters in a string.

"""

def wonderfulSubstrings(s):
    """
    :type s: str
    :rtype: int
    """
    wonderful = 0

    for i in range(len(s)):
        hash = {}

        for j in range(i,len(s)):
            string = (s[i:j+1])
            count = 0
            if s[j] not in hash:
                hash[s[j]] = 0
            hash[s[j]] += 1

            for key in hash:
                if hash[key] % 2 == 1:
                    count += 1

            if count <= 1:
                wonderful += 1

    return wonderful

string = "he"


print(wonderfulSubstrings(string))
