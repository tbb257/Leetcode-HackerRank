"""
Link: https://leetcode.com/problems/daily-temperatures/

Given an array of integers temperatures represents the daily temperatures, return an array answer such that answer[i]
is the number of days you have to wait after the ith day to get a warmer temperature.

If there is no future day for which this is possible, keep answer[i] == 0 instead.
"""

def dailyTemperatures(temperatures):

    #____________________________Brute Force_____________________#
    # result = []
    # def bfs (temperature, stack):
    #     for i in range(len(stack)):
    #         if stack[i] > temperature:
    #             difference = i+1
    #             result.append(difference)
    #             return
    #         elif i == len(stack)-1:
    #             result.append(0)

    # for i in range(len(temperatures)):
    #     stack = temperatures[i+1:len(temperatures)]
    #     bfs(temperatures[i],stack)

    # result.append(0)
    #____________________________________________________________#


    result = [0]*len(temperatures)
    stack = []

    for i,v in enumerate(temperatures):
        # Instead of enumerating, could also just do for-loop for i in range(len(temperatures)) and declare v = temperature[i]
        while stack and stack[-1][1] < v:
            index,difference = stack.pop()
            result[index] = i - index
        stack.append([i, v])

    return result

test = [73,74,75,71,69,72,76,73]
print(dailyTemperatures(test))
