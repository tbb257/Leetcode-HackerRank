"""
Link: https://leetcode.com/problems/group-anagrams/

Given an array of strings strs, group the anagrams together. You can return the answer in any order.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.


"""
def groupAnagrams(strs):
    """
    :type strs: List[str]
    :rtype: List[List[str]]

    Psuedocode
    Result = []
    create hash map
    for each word in the string:
        Convert word into a sorted list, then use ''.join to make it a string of ordered characters (all anagram should be equal)
        Add this ordered string to the hash map as key, value will be list of anagrams
        Each time an iteration is a similar anagram, append it. Else create new key for unique anagram
    perform for-loop of hash map where you append each list to result list
    return list
    """
    hash = {}
    result = []
    for word in strs:
        ordered = ''.join(sorted(word))
        if ordered not in hash:
            hash[ordered] = [word]
        else:
            hash[ordered].append(word)
    for key in hash:
        result.append(hash[key])

    return result

strs = ["eat","tea","tan","ate","nat","bat"]

print(groupAnagrams(strs))
