"""
Link: https://leetcode.com/problems/longest-consecutive-sequence/description/

Given an unsorted array of integers nums, return the length of the longest consecutive elements sequence.

You must write an algorithm that runs in O(n) time.

"""

def longestConsecutive(nums):
    """
    :type nums: List[int]
    :rtype: int
    """
    if nums == []:
        return 0
    if len(nums) == 1:
        return 1
    hash = {}
    nums.sort()
    curr = nums[0]
    sequence = [nums[0]]
    for i in range(1, len(nums)):
        if nums[i] == curr + 1:
            sequence.append(nums[i])
        elif nums[i] == curr:
            continue
        else:
            hash[len(sequence)] = sequence
            sequence = [nums[i]]
        curr = nums[i]

    hash[len(sequence)] = sequence
    sequence = [nums[i]]
    return(max(hash))

test = [1,2,0,1]
print(longestConsecutive(test))


"""
Edge cases missed and revised for
If nums == [], needed to return 0. Original code would return error for line 17
If len(nums), needed to return 1. Original code would return error for range in line 19
If iteration == curr, needed to avoid breaking the loop. Added a continuation for this scenario.

"""
