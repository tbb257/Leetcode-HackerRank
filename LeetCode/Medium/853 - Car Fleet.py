"""
Link: https://leetcode.com/problems/car-fleet/

There are n cars going to the same destination along a one-lane road. The destination is target miles away.

You are given two integer array position and speed, both of length n, where position[i] is the position of the ith car and speed[i] is the speed of the ith car (in miles per hour).

A car can never pass another car ahead of it, but it can catch up to it and drive bumper to bumper at the same speed. The faster car will slow down to match the slower car's speed. The distance between these two cars is ignored (i.e., they are assumed to have the same position).

A car fleet is some non-empty set of cars driving at the same position and same speed. Note that a single car is also a car fleet.

If a car catches up to a car fleet right at the destination point, it will still be considered as one car fleet.

Return the number of car fleets that will arrive at the destination.
"""

def carFleet(target, position, speed):
    """
    :type target: int
    :type position: List[int]
    :type speed: List[int]
    :rtype: int
    """
    array = [(position[i], speed[i]) for i in range(len(position))]
    array.sort(reverse=True)
    stack = []

    for position, speed in array:
        steps = int((target - position) / speed)
        while stack and stack[0][2] >= steps:
            stack.pop(0)
            speed = min(speed, stack[0,2])
            #some code to calc new position
            steps = int((target - position) / speed)
            fleet = (position,speed,steps)
            stack.insert(0, fleet)
        stack.append(position, speed, steps)
    return array

target = 12
position = [10,8,0,5,3]
speed = [2,4,1,1,3]

print(carFleet(target, position, speed))
