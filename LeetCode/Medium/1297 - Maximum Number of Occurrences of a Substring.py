"""
Link: https://leetcode.com/problems/maximum-number-of-occurrences-of-a-substring/
Given a string s, return the maximum number of occurrences of any substring under the following rules:

The number of unique characters in the substring must be less than or equal to maxLetters.
The substring size must be between minSize and maxSize inclusive.
"""

def maxFreq(s, maxLetters, minSize, maxSize):
    """
    :type s: str
    :rtype: int
    """
    hash = {}
    for i in range(len(s)):
        j = i
        maxChar = 0
        substring = ""
        while (j-i) <= maxSize and maxChar <= maxLetters and j < len(s):
            if s[j] not in substring:
                maxChar += 1
            if maxChar > maxLetters:
                continue

            substring += s[j]

            if len(substring) >= minSize:
                if substring not in hash:
                    hash[substring] = 0
                hash[substring] += 1
            j += 1


    if hash:
        return max(hash.values())
    else:
        return 0


string = "aababcaab"
maxLetters = 2
minSize = 3
maxSize = 3


print(maxFreq(string, maxLetters, minSize, maxSize))
