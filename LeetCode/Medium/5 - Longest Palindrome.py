def longestPalindrome(s):
    """
    :type s: str
    :rtype: str
    """
    palindrome = ""
    lengthpal = 0
    for i in range(len(s)):
        # odd length string
        left,right = i,i
        while (left >= 0 and right < len(s)) and s[left] == s[right]:
            if (right-left+1) > lengthpal:
                palindrome = s[left:right+1]
                lengthpal = right-left+1
            left -=1
            right += 1

        #even length string
        left,right = i,i+1
        while (left >= 0 and right < len(s)) and s[left] == s[right]:
            if (right-left+1) > lengthpal:
                palindrome = s[left:right+1]
                lengthpal = right-left+1
            left -=1
            right += 1
    return palindrome

test = "babac"
print(longestPalindrome(test))


"""
Logic
For each iteration, expand outwards in both directions from the letter. For string to be palindromic, left edge must equal right edge at all times.
Loop only occurs if left does not go below 0, and right stays less than length of string
If Above is true and s[left] == s[right]:
    Check legnth of string (right-left + 1)
    If it's greater than lengthpal (first palindrome one will be, by default)
    Change palindrome string and update lengthpal
Shift left-1 and right+1

Repeat for even length strings, but don't make left and right teh same character initially. Make right == i+1

"""
