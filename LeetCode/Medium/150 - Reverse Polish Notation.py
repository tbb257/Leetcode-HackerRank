"""
Link: https://leetcode.com/problems/evaluate-reverse-polish-notation/

You are given an array of strings tokens that represents an arithmetic expression in a Reverse Polish Notation.

Evaluate the expression. Return an integer that represents the value of the expression.

Note that:

The valid operators are '+', '-', '*', and '/'.
Each operand may be an integer or another expression.
The division between two integers always truncates toward zero.
There will not be any division by zero.
The input represents a valid arithmetic expression in a reverse polish notation.
The answer and all the intermediate calculations can be represented in a 32-bit integer.
"""
def evalRPN(tokens):
    """
    :type tokens: List[str]
    :rtype: int
    """
    stack = []
    # for char in tokens:
    #     if char == "+":
    #         value = stack.pop() + stack.pop()
    #         stack.append(value)
    #     elif char == "-":
    #         a,b = stack.pop(), stack.pop()
    #         value = b - a
    #         stack.append(value)
    #     elif char == "*":
    #         value = stack.pop() * stack.pop()
    #         stack.append(value)
    #     elif char == "/":
    #         a, b = stack.pop(), stack.pop()
    #         value = b / a
    #         stack.append(int(value))
    #     else:
    #         stack.append(int(char))
    operators = "+-/*"
    for char in tokens:
        if char not in operators:
            stack.append(int(char))
        else:
            right, left = stack.pop(), stack.pop()
            if char == "+":
                value = left + right
            elif char == "-":
                value = left - right
            elif char == "*":
                value = left * right
            else:
                value = int(left/right)
            stack.append(value)

    return stack[0]

test = ["10","6","9","3","+","-11","*","/","*","17","+","5","+"]
print(evalRPN(test))
