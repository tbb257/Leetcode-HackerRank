"""
Link: https://leetcode.com/problems/score-of-parentheses/

Given a balanced parentheses string s, return the score of the string.

The score of a balanced parentheses string is based on the following rule:

"()" has score 1.
AB has score A + B, where A and B are balanced parentheses strings.
(A) has score 2 * A, where A is a balanced parentheses string.

"""

def scoreOfParentheses(s):
    """
    :type s: str
    :rtype: int
    """

    stack = []
    score = 0
    index = -1
    for char in s:
        if char == "(":
            stack.append(score)
            score = 0
        else:
            nest = stack.pop()
            score = nest + max(1, 2*score)
    return score

string = "((()())())"


print(scoreOfParentheses(string))
