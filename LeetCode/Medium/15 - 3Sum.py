"""
Link: https://leetcode.com/problems/3sum/

Given an integer array nums, return all the triplets [nums[i], nums[j], nums[k]] such that i != j, i != k, and j != k, and nums[i] + nums[j] + nums[k] == 0.

Notice that the solution set must not contain duplicate triplets.
"""

def threeSum(nums):
    """
    :type nums: List[int]
    :rtype: List[List[int]]
    """
    nums.sort()
    results = []
    for i in range(len(nums)-2):
        if i > 0 and nums[i] == nums[i-1]:
            continue

        left, right = i+1, len(nums)-1

        while left < right:
            threeSum = nums[i] + nums[left] + nums[right]
            if threeSum > 0:
                right -= 1
            elif threeSum < 0:
                left += 1
            else:
                triplet = [[nums[i], nums[left], nums[right]]]
                if triplet not in results:
                    results.append([nums[i], nums[left], nums[right]])

                left += 1
                if nums[left] == nums[left-1] and left < right:
                    left += 1
    return results

nums = [-1,0,1,2,-1,-4]
print(threeSum(nums))
