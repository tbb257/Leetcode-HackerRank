"""
Link: https://leetcode.com/problems/sum-of-beauty-of-all-substrings/
The beauty of a string is the difference in frequencies between the most frequent and least frequent characters.

For example, the beauty of "abaacc" is 3 - 1 = 2.
Given a string s, return the sum of beauty of all of its substrings.
"""

def beautySum(s):
    """
    :type s: str
    :rtype: int
    """
    beauty = 0

    for i in range(len(string)):
        j = i
        hash = {}
        while j < len(string):
            if string[j] not in hash:
                hash[string[j]] = 0
            hash[string[j]] += 1
            beauty += (max(hash.values()) - min(hash.values()))
            j += 1

    return beauty

    """
    Old Solution

        for i in range(len(s)):
        j = i
        while j < len(s):
            hash = {}
            substring = s[i:j+1]
            for char in substring:
                if char not in hash:
                    hash[char] = 0
                hash[char]+=1
            beauty.append(max(hash.values()) - min(hash.values()))
            j += 1
    return sum(beauty)


    You don't need a nested for-loop that creates substrings. Just slide j over 1 index and check next character in string. Add to hash key of that string, calculate beauty again and just add it to list.
    Don't actually even need a list either, just add it to an initialized value
    """


string = "aabcbaa"


print(beautySum(string))
