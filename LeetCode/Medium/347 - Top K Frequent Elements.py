"""
Link: https://leetcode.com/problems/top-k-frequent-elements/

Given an integer array nums and an integer k, return the k most frequent elements. You may return the answer in any order.


"""
def topKFrequent(nums, k):
    """
    :type nums: List[int]
    :type k: int
    :rtype: List[int]

    """
    count = {}
    freq = [[] for i in range(len(nums)+1)] # freq array can at most be as long as length of nums array
    result = []

    for value in nums:
        if value not in count: count[value] = 0
        count[value] += 1
    for num, count in count.items(): # Returns the key-value pairs - note that the key is the value in nums, the value is its frequency
        freq[count].append(num)

    for i in range(len(freq)-1, 0, -1): # Start backwards since you want the most frequent elements
        for number in freq[i]:
            result.append(number) # If freq[i] is an empty list, then nothing appends. Only appends once the freq index contains a list of actual values
            if len(result) == k:
                return result



test = [1]
k = 1


print(topKFrequent(test,k))
