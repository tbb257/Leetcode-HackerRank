"""
Link: https://leetcode.com/problems/3sum-closest/

Given an integer array nums of length n and an integer target, find three integers in nums such that the sum is closest to target.

Return the sum of the three integers.

You may assume that each input would have exactly one solution.
"""
def threeSumClosest(nums, target):
    """
    :type nums: List[int]
    :type target: int
    :rtype: int
    """
    closest = float("inf")
    sums = {}
    nums.sort()

    for i in range(len(nums)):
        left = i+1
        right = len(nums)-1
        while left < right:
            threeSum = nums[i] + nums[left] + nums[right]
            proximity = abs(threeSum - target)
            sums[proximity] = threeSum
            if proximity > closest:
                right -= 1
            elif proximity <= closest:
                closest = proximity
                left += 1
    print("sums,", sums)
    print("closest,", closest)

    return sums[closest]

[-5, 3, 0]
nums = [4,0,5,-5,3,3,0,-4,-5]
target = -2

print(threeSumClosest(nums, target))

