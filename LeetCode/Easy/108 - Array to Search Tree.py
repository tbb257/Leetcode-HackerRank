class Solution(object):
    def sortedArrayToBST(self, nums):
        """
        :type nums: List[int]
        :rtype: TreeNode
        """
        def helper(l, r):
            if l > r:
                return None
            mid = (l+r)//2
            root = TreeNode(nums[mid])
            root.left = helper(l, (mid-1))
            root.right = helper((mid+1), r)
            return root

        left_end = 0
        right_end = len(nums)-1

        return helper(left_end, right_end)
