"""
Link: https://leetcode.com/problems/valid-parentheses/

Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:

Open brackets must be closed by the same type of brackets.
Open brackets must be closed in the correct order.
Every close bracket has a corresponding open bracket of the same type.

"""

def isValid(s):
    """
    :type s: str
    :rtype: bool

    """

    hash = {"(":")", "[":"]", "{":"}"}
    stack = []

    for char in s:
        if char in "([{":
            stack.append(char)
        elif len(stack) == 0:
            return False
        else:
            openTag = stack.pop()
            if char != hash[openTag]:
                return False
    return len(stack) == 0


test = "()[[[()]]]{}"

print(isValid(test))
