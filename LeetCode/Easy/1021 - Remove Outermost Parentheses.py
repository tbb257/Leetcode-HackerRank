"""
Link: https://leetcode.com/problems/remove-outermost-parentheses/

A valid parentheses string is either empty "", "(" + A + ")", or A + B, where A and B are valid parentheses strings, and + represents string concatenation.

For example, "", "()", "(())()", and "(()(()))" are all valid parentheses strings.
A valid parentheses string s is primitive if it is nonempty, and there does not exist a way to split it into s = A + B, with A and B nonempty valid parentheses strings.

Given a valid parentheses string s, consider its primitive decomposition: s = P1 + P2 + ... + Pk, where Pi are primitive valid parentheses strings.

Return s after removing the outermost parentheses of every primitive string in the primitive decomposition of s.

"""

def removeOuterParentheses(s):
    """
    :type s: str
    :rtype: bool

    """
    """
    Make new string
    If char is (, append to stack
    If char is ),
        open = stack.pop()
    Add char to basket
    If stack is ever empty
        Append basket to result (except for outermost characters -> basket[1:len(basket)-1] or basket[1:-1])
    return new string

    """
    result = ""
    basket = ""
    stack = []
    for char in s:
        if char == "(":
            stack.append(char)
        else:
            stack.pop()
        basket += char

        if not stack:
            result += basket[1:-1]
            basket = ""
    return result


test = "(()())(())"

print(removeOuterParentheses(test))
