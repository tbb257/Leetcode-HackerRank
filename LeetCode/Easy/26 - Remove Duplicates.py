class Solution(object):
    def removeDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        i = 1
        while i<len(nums):
            if nums[i] == nums[i-1]:
                nums.pop(i)
            else:
                i+=1
        return len(nums)
        # unique = []
        # for i in range(len(nums)):
        #     if nums[i] not in unique:
        #         unique.append(nums[i])
        #     else:
        #         nums.pop(i)
        # return len(nums)
        """
        This code didn't work because line 15 takes snapshot of length.However as you pop values our,
        length decreases but initial range stays the same, so eventually the range will be out of list index
        """
