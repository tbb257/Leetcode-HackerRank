"""
Link: https://leetcode.com/problems/merge-two-sorted-lists/

You are given the heads of two sorted linked lists list1 and list2.

Merge the two lists in a one sorted list. The list should be made by splicing together the nodes of the first two lists.

Return the head of the merged linked list.

"""

class ListNode(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

    def mergeTwoLists(self, list1, list2):
        """
        :type list1: Optional[ListNode]
        :type list2: Optional[ListNode]
        :rtype: Optional[ListNode]
        """
        res = ListNode  # Create a new ListNode
        curr = res  #Create the pointer
        while list1 and list2: # Will keep executing the code below until one of the lists are now None
            if list1.val < list2.val: #Check which value is greater
                curr.next = list1 #If list1 is greater, add it as the next to the current node
                list1 = list1.next #Slide into the next value for L1
            else:
                curr.next = list2 #Same as above but for L2
                list2 = list2.next
            curr = curr.next
        curr.next = list1 or list2 #Once loop breaks, one of the lists is None. Assuming the other might still have data, add it to end of curr
        return res #Return res, not curr. curr is just the pointer
