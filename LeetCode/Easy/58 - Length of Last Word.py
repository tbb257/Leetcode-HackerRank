class Solution(object):
    def lengthOfLastWord(self, s):
        """
        :type s: str
        :rtype: int
        """
        sentence = s.split()
        print(sentence)
        return len(sentence[len(sentence)-1])
