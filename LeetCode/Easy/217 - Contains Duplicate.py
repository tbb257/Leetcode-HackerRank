"""
Link: https://leetcode.com/problems/contains-duplicate/
Given an integer array nums, return true if any value appears at least twice in the array, and return false if every element is distinct.


"""
def containsDuplicate(self, nums):
    """
    :type nums: List[int]
    :rtype: bool
    """
    counter = {}
    for value in nums:
        if value not in counter:
            counter[value] = 0
        else:
            return True
    return False



    """
    Original solution was to simply run a nums.count(value) for each iteration.
    This takes too much time as nums.count scans through entire list for each iteration. Becomes O(N^2)
    """
