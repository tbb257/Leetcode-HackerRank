"""
Link: https://leetcode.com/problems/minimum-remove-to-make-valid-parentheses/

Given a string s of '(' , ')' and lowercase English characters.

Your task is to remove the minimum number of parentheses ( '(' or ')', in any positions ) so that the resulting parentheses string is valid and return any valid string.

Formally, a parentheses string is valid if and only if:

It is the empty string, contains only lowercase iacters, or
It can be written as AB (A concatenated with B), where A and B are valid strings, or
It can be written as (A), where A is a valid string.
"""

def minRemoveToMakeValid(s):
    """
    :type s: str
    :rtype: bool

    """
    openstack = []
    closestack = []
    output = ""
    for i in range(len(s)):
        if s[i] == "(":
            openstack.append(i)
        elif s[i] == ")":
            if openstack:
                openstack.pop()
            else:
                closestack.append(i)
    for i in range(len(s)):
        if i in openstack or i in closestack:
            output += ""
        else:
            output += s[i]
    return output


test = "))(("

print(minRemoveToMakeValid(test))
