class Solution(object):
    def plusOne(self, digits):
        """
        :type digits: List[int]
        :rtype: List[int]
        """
        string = ""
        for digit in digits:
            string += str(digit)
        plus_one = int(string) + 1
        res = []
        for digit in str(plus_one):
            res.append(int(digit))
        return res
