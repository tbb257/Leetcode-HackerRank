"""
Link: https://leetcode.com/problems/matrix-diagonal-sum/
Given a square matrix mat, return the sum of the matrix diagonals.

Only include the sum of all the elements on the primary diagonal and all the elements on the secondary diagonal that are not part of the primary diagonal.
"""
def diagonalSum(mat):
    """
    :type mat: List[List[int]]
    :rtype: int
    """
    sum = 0
    for i in range(len(mat)):
        sum += mat[i][i]
        if (len(mat)-i-1) != i:
            sum += mat[i][len(mat)-i-1]
    return sum



test = [[5]]

print(diagonalSum(test))
