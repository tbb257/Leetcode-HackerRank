"""
Link: https://leetcode.com/problems/valid-palindrome/
A phrase is a palindrome if, after converting all uppercase letters into lowercase letters and removing all non-alphanumeric characters, it reads the same forward and backward.
Alphanumeric characters include letters and numbers.
Given a string s, return true if it is a palindrome, or false otherwise.
"""

def isPalindrome(s):
    """
    :type s: str
    :rtype: bool
    """
    left = 0
    right = len(s)-1
    while left < right:
        while not s[left].isalnum():
            left += 1
        while not s[right].isalnum():
            right -= 1
        if s[left].lower() != s[right].lower():
            return False
        else:
            left += 1
            right -= 1
    return True

input = "A man, a plan, a canal: Panama"
print(isPalindrome(input))
