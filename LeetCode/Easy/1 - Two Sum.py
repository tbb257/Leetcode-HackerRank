"""
Link: https://leetcode.com/problems/two-sum/

Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.


"""

def twoSum(nums, target):
    """
    :type nums: List[int]
    :type target: int
    :rtype: List[int]
    """
    hash = {nums[0]:0}
    for i in range(1,len(nums)):
        difference = target - nums[i]
        if difference in hash:
            return [hash[difference], i]
        else:
            hash[nums[i]] = i

    """
    Original solution:
    def twoSum(self, nums, target):

        :type nums: List[int]
        :type target: int
        :rtype: List[int]

        for i in range(len(nums)):
            for j in range(i+1,len(nums)):
                if nums[i] + nums[j] == target:
                    return [i,j]

    Original solution takes O(N^2) time for nested foor loop. Using hash table saves time.
    """


nums = [2,7,11,15]
target = 9

print(twoSum(nums,target))
