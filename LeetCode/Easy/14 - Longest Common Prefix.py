"""
Link: https://leetcode.com/problems/longest-common-prefix/

Write a function to find the longest common prefix string amongst an array of strings.

If there is no common prefix, return an empty string "".


"""


def longestCommonPrefix(strs):
    """
    :type strs: List[str]
    :rtype: str

    Pseudocode:
    Make the first word the default prefix.
    Do a for-loop going through the list:
        While the prefix is not equal to the word in the loop:
            Reduce the length of the prefix string by 1
            Once the prefix equals to the word in the loop, the prefix should stop deleting and continue loop
            If prefix ever equals "", return it automatically


    """
    prefix = strs[0]
    for string in strs:
        while prefix != string[:len(prefix)]:
            prefix = prefix[:-1]
            if prefix == "":
                return prefix
    return prefix
