class Solution(object):
    def strStr(self, haystack, needle):
        """
        :type haystack: str
        :type needle: str
        :rtype: int
        """

        for i in range(len(haystack)):
            if haystack[i] == needle[0]:
                if haystack[i:(i+len(needle))] == needle:
                    return i
        return -1

        # Using nested for loop
        def test(haystack, needle):
            for i in range(len(haystack)):
                for j in range(i, len(haystack)):
                    string = haystack[i:j]
                    if needle == string:
                        return i
            return -1
