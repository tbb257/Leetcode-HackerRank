"""
Link: https://leetcode.com/problems/valid-anagram/

Given two strings s and t, return true if t is an anagram of s, and false otherwise.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.


"""
def isAnagram(s, t):
    """
    :type s: str
    :type t: str
    :rtype: bool
    """
    if len(s) != len(t):
        return False

    list1 = list(s)
    list2 = list(t)
    list1.sort()
    list2.sort()

    #Just typing list1 = sorted(s) will execute lines 19 and 21 simultaneously

    for i in range(len(list1)):
        if list1[i] != list2[i]:
            return False
    return True


    """
    Original solution was to simply run a nums.count(value) for each iteration.
    This takes too much time as nums.count scans through entire list for each iteration. Becomes O(N^2)
    """

s = "anagram"
t = "nagaram"

print(isAnagram(s,t))
