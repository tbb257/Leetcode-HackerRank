class Solution(object):
    def mySqrt(self, x):
        """
        :type x: int
        :rtype: int
        """

        start = 0
        end = x

        while start <= end:
            mid = (start+end)//2
            squared = mid*mid
            if squared == x or (squared > x and ((mid-1)*(mid-1)) < x):
                return mid
            elif squared > x:
                end = mid-1
            else:
                start = mid+1
