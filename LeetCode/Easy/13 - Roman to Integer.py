"""
Link: https://leetcode.com/problems/roman-to-integer/

Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.
Given a roman numeral, convert it to an integer.


"""


def romanToInt(s):
    """
    :type s: str
    :rtype: int
    """
    roman = {"I":1, "V":5, "X":10, "L":50, "C":100, "D":500, "M":1000}
    total = 0

    modified = s.replace("IV", "IIII").replace("IX","VIIII").replace("XL","XXXX").replace("XC","LXXXX").replace("CD",    "CCCC").replace("CM","DCCCC")
    for char in modified:
        total += roman[char]
    return total


test = "MCMXCIV"
print(romanToInt(test))
