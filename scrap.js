class Student {
    constructor(firstName, lastName, grade){
        this.firstName = firstName;
        this.lastName = lastName;
        this.grade = grade;
        this.tardy = 0;
        this.scores = [];
    }
    fullName(){
        console.log(`This student's name is ${this.firstName} ${this.lastName}`)
    }
    markLate(){
        this.tardy += 1;
        if (this.tardy <= 3){
            console.log(`${this.firstName} ${this.lastName} was late today. This student has now been tardy ${this.tardy} time(s).`);
        }else{
            console.log(`${this.firstName} ${this.lastName} has now been late ${this.tardy} times, which exceed the maximum of 3. This student is now marked for expulsion.`);
        }
    }
    addScore(score){
        this.scores.push(score);
        console.log(`${this.firstName} now has ${this.scores.length} grades`)
    }
    calculateAverage(){
        let sum = 0;
        for (let i = 0; i < this.scores.length; i++){
            sum += this.scores[i]
        }
        let average = sum/this.scores.length;
        console.log(`${this.firstName} currently has an average of ${average}`)
    }
}


let student = new Student("Michael", "Gambit")
student.fullName()
student.addScore(100)
student.addScore(50)
student.calculateAverage()
