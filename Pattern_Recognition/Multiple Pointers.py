
"""
SumZero
Given sorted array, find first pair whose sum = 0
"""
def sumZero(array):
    curr_left = 0
    curr_right = len(array)-1

    while curr_left != curr_right:
        sum = array[curr_left] + array[curr_right]
        if sum == 0:
            return [array[curr_left], array[curr_right]]
        elif sum > 0:
            curr_right -= 1
        else:
            curr_left += 1
    return False


arr1 = [-4,-3,-2,-1,0,1,2,3,10]


# print(sumZero(arr1))

"""
countUniqueValues
Implement function which takes in sorted array and counts unique values in array
"""

def countUniqueValues(array):
    if array == []:
        return 0

    marker = 0
    curr = 1
    unique = 1

    while marker != len(array) and curr != len(array):
        last = array[marker]
        check = array[curr]
        if check != last:
            unique += 1
            marker = curr
        curr += 1

    return unique

test = [-2,-1,-1,0,1]
print(countUniqueValues(test))
