
"""
Basic Frequency Counter
Confirm that for each value in array1, the frequency of this value is equal to the frequency of the value squared in array2.
"""
def squaredfrequency(arr1, arr2):
    frequency = {}
    for i in range(len(arr1)):
        if arr1[i] not in frequency:
            frequency[arr1[i]] = 0
        if arr2[i] not in frequency:
            frequency[arr2[i]] = 0
        frequency[arr1[i]] += 1
        frequency[arr2[i]] += 1

    for value in arr1:
        if (value**2) not in frequency:
            return False
        if frequency[value] != frequency[value**2]:
            return False
    return True

arr1 = [1,2,2,5,6]
arr2 = [4,4,1,36,25]

# print(squaredfrequency(arr1, arr2))

"""
Palindrome Frequency Counter
Confirm that the string has some variation of an Palindrome
"""

def PalindromePermutation(string):
    frequency = {}
    oddCounter = 0

    for char in string.lower():
        if char == " ":
            continue
        elif char not in frequency:
            frequency[char] = 0
        frequency[char] += 1

    for key in frequency:
        if frequency[key] % 2  != 0:
            oddCounter += 1
        if oddCounter > 1:
            return False

    return True

testcase = "Tact Coa"
# rint(PalindromePermutation(testcase))
