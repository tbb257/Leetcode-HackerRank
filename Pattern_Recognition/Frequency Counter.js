


function validAnagram(string1, string2){
    let object1 = {};
    let object2 = {};

    for (let i = 0; i < string1.length; i++){
        if (!(string1[i] in object1)){
            object1[string1[i]] = 0
        }
        if (!(string2[i] in object2)){
            object2[string2[i]] = 0
        }

        object1[string1[i]] += 1
        object2[string2[i]] += 1
    }


    for (const key in object1){
        if (!(key in object2)){
            return false
        }
        if (!(object1[key] == object2[key])){
            return false
        }
    }
    return true
}

let string1 = "anagram"
let string2 = "nagaram"

console.log(validAnagram(string1, string2))
