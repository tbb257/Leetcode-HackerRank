
"""
Max Sub Array
Calculate maximum sum of 'n' consecutive integers
"""
def maxSubarraySum(array, n):
    if array == []:
        return None

    total = sum(array[0:n])
    max = total
    for i in range(n, len(array)):
        total = total - array[i-n] + array[i]
        if total > max:
            max = total
    return max


test1 = [1,2,5,2,8,1,5]
n1 = 4

test2 = [4,2,1,6,2]
n2 = 4
print(maxSubarraySum(test1, n1))
print(maxSubarraySum(test2, n2))
