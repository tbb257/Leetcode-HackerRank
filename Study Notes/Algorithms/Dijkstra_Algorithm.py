""""
Weighted Graph used to calculate shortest path between two nodes
"""
class WeightedGraph:
    def __init__(self):
        self.adjacency_list = {}

    def add_vertex(self, vertex):
        if vertex not in self.adjacency_list:
            self.adjacency_list[vertex] = []
        return self

    def add_edge(self, v1, v2, weight):
        self.adjacency_list[v1].append({"node":v2, "weight":weight})
        self.adjacency_list[v2].append({"node":v1, "weight":weight})
        return self


graph = WeightedGraph()
points = ["A", "B", "C", "D", "E", "F"]
for char in points:
    graph.add_vertex(char)
graph.add_edge("A", "B", 4)
graph.add_edge("A", "C", 2)
graph.add_edge("B", "E", 3)
graph.add_edge("C", "D", 2)
graph.add_edge("C", "F", 4)
graph.add_edge("D", "E", 3)
graph.add_edge("D", "F", 1)
graph.add_edge("E", "F", 1)


print(graph.adjacency_list)
