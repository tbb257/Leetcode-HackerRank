"""
Merging Algorithm
Pseudocode --> O(n+m) time and space
    Create an empty array to be returned
    Look at smallest values of each sorted array (use while loop)
    While there are still values to look at:
        Take value of first array and compare to value of second
        If first is smaller, input into new array and move pointer for first array
        Or Vice versa

        Once one array is finished, push rest of second to new array at end

"""

def merge(arr1, arr2):
    sorted = []
    i,j = 0,0
    while i<len(arr1) and j<len(arr2):
        if arr1[i] < arr2[j]:
            sorted.append(arr1[i])
            i+=1
        else:
            sorted.append(arr2[j])
            j+=1

    if i==len(arr1):
        sorted += arr2[j:]
    else:
        sorted += arr1[i:]

    return sorted


arr1 = [1,10,50]
arr2 = [2,14,99,100]
# print(merge(arr1,arr2))

"""
MergeSort Algorithm
MOST USE RECURSION
    Pseudocode
    Break up array into halves continuously until each array is empty or has one element
    Once you have smaller arrays (sorted by default now) merge those arrays with one another until you're back at full length
    Return the now merged (and sorted) array

    O(n logn) time complexity
    O(n) space complexity

"""
def mergeSort(arr):
    # represents base case for the recursions
    if len(arr) <= 1:
        return arr

    mid = len(arr)//2

    # left and right represent halves of the array. Equal to the recursion of you splitting halves continuously
    # return left and right would present:
        # (([1], ([10], [50])), (([2], [14]), ([99], [100])))
        # now we simply call sort on left and right

    left = mergeSort(arr[0:mid])
    right = mergeSort(arr[mid:])

    return merge(left,right)

arr = arr1 + arr2
print(mergeSort(arr))
