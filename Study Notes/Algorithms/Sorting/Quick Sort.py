"""
Pivot Algorithm
Psuedocode
Given an array, start index and end index, designate an element as the pivot
    Start index can default to 0, end index to array length-1
Scan through entire array, and rearrange values based on whether they're less or greater than pivot element.
    Order on each side of pivot doesn't matter
    Helper is to do this in place, modifying existing array instead of creating new
    Helper shall return index of the pivot, which is equal to the number of values less than the pivot element


"""

def pivot(array, low, high):

    pointer = array[high]
    swapIndex = low-1

    for i in range(low, high):
        if array[i] <= pointer:
            # if value is less than pointer, increment tracker to tell you where pivot will eventually go
            # as condition is met, switch the smaller values with the swap index. 'i' represent index of current check value, swapIndex represent left side of eventual pivot
            # recommend creating a small helper function that swaps, since you will use it more than once
            swapIndex += 1
            array[swapIndex], array[i] = array[i], array[swapIndex]

    (array[swapIndex+1], array[high]) = (array[high], array[swapIndex+1])
    return swapIndex+1


array = [4,8,2,1,5,7,6,3]
# Given array of [4, 8, 2, 1, 5, 7, 6, 3]
# Should return [3, 2, 1, 4, 5, 7, 6, 8] because 4 is read as pivot
# print(pivot(array))

"""
Quicksort Algorithm
Call pivot helper on array recursively
    For each recursion, call pivot on left and right half of last pivot index.
        --> This is why the helper function returns the swapIndex, so you can call Pivot recursively on left and right halves
    Base case occurs when you consider subarrays less than 2 elements

"""
def quickSort(array, low, high):
    if low < high:
        pivotIndex = pivot(array, low, high)
        quickSort(array, low, pivotIndex-1)
        quickSort(array, pivotIndex+1, high)

    return array

left = 0
right = len(array)-1
print(quickSort(array,left,right))
