""""
Binary Search Code
    Efficient Method compared to linear search, which is at least O(N) in time complexity

    Create a left pointer at the start of the array, and right pointer and the end of the array (In array length of 10, left is 0 and right is 9)
    Loop through the array to see if you have the element
        Add condition that checks that left is still less than right
        Create a pointer for middle of the array
            If the value in this middle is too small, move the left pointer up
            If the value in the current middle is too large, move the right pointer down
            New middle is checked for condition

    Idea isn't to move the middle pointer directly, but the decrease the window of the array [left index ..... middle index ..... right index] by changing left or right index continuously
"""

def binarySearch(arr, value):
    left = 0
    right = len(arr)-1

    while left <= right:
        middle = (right+left)//2
        if arr[middle] < value:
            left += 1
        elif arr[middle] > value:
            right -= 1
        else:
            return middle

    return False


test = [5, 6, 10, 13, 14, 18, 30, 34, 35, 37,
  40, 44, 64, 79, 84, 86, 95, 96, 98, 99]
value = 30

print(binarySearch(test, value))
