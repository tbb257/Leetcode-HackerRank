"""
Write a function called power which accepts a base and an exponent.
The function should return the power of the base to the exponent.
This function should mimic the functionality of Math.pow()  - do not worry about negative bases and exponents.
"""

def PowerRecursive(base, exponent):
    if exponent == 1:
        return base
    result = base * PowerRecursive(base, (exponent-1))

    return result


base = 10
exponent = 4

# print(PowerRecursive(base,exponent))
"""
    result = base * PowerRecursive(base, (exponent-1))
    result = 10 * PowerRecursive(10, 3)
                  10 * PowerRecursive(10, 2)
                       10 * PowerRecursive(10, 1)
                       10 * 10 == 100
                  10 * 100 == 1000
             10 * 1000 == 10000
    Stack completes and result is equal to 1, which is returned in line 12
"""

#_______________________________________________________________________________________________________________________________________________________________#


"""
Write a function factorial which accepts a number and returns the factorial of that number.
A factorial is the product of an integer and all the integers below it;
e.g., factorial four ( 4! ) is equal to 24, because 4 * 3 * 2 * 1 equals 24.  factorial zero (0!) is always 1.
"""

def FactorialRecursive(number):
    if number == 0:
        return 1
    return number * FactorialRecursive(number-1)



number = 5
# print(FactorialRecursive(number))

"""
    return number * FactorialRecursive(number-1)
           5 * FactorialRecursive(4)
               4 * FactorialRecursive(3)
                   3 * FactorialRecursive(2)
                       2 * FactorialRecursive(1)
                           1 * FactorialRecursive(0)
                           1 * 1 == 1
                       2 * 1 == 2
                   3 * 2 == 6
               4 * 6 == 24
           5 * 24 == 120
    return 120
"""

#_______________________________________________________________________________________________________________________________________________________________#

"""
Write a function called productOfArray which takes in an array of numbers and returns the product of them all.
"""
def ProductArrayRecursive(array):
    if len(array) == 1:
        return array[0]

    return array[0] * ProductArrayRecursive(array[1:])

array = [1,2,3,10]
# print(ProductArrayRecursive(array))

#_______________________________________________________________________________________________________________________________________________________________#

"""
Write a function called recursiveRange which accepts a number and adds up all the numbers from 0 to the number passed to the function.
"""

def RangeRecursive(number):
    if number == 1:
        return number
    return number + RangeRecursive(number-1)

number = 10
# print(RangeRecursive(number))

#_______________________________________________________________________________________________________________________________________________________________#


"""
Write a recursive function called reverse which accepts a string and returns a new string in reverse.
"""

def ReverseStringRecursive(string):
    if len(string) == 1:
        return string
    return string[-1] + ReverseStringRecursive(string[0:len(string)-1])

string = "rithmschool"
# print(ReverseStringRecursive(string))

#_______________________________________________________________________________________________________________________________________________________________#

"""
Write a recursive function called isPalindrome which returns true if the string passed to it is a palindrome (reads the same forward and backward).
Otherwise it returns false.
"""

def PalindromeRecursion(string):
    def ReverseString(word):
        if len(word) == 1:
            return word
        return word[-1] + ReverseString(word[0:len(word)-1])

    test = ReverseString(string)

    return string == test

string = "amanaplanacanalpanama"
# print(PalindromeRecursion(string))

#_______________________________________________________________________________________________________________________________________________________________#

"""
Write a recursive function called someRecursive which accepts an array and a callback.
The function returns true if a single value in the array returns true when passed to the callback.
Otherwise it returns false.
"""

def someRecursive(array, callback):
    if callback(array) == True:
        return True
    elif len(array) == 1:
        return callback(array)

    return someRecursive(array[1:len(array)])

array = [1,2,3,4]
"""
Callback function will not be created. Simply create condition for true/false check
"""
