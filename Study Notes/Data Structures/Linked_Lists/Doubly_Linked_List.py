class Node:
    def __init__(self, val):
        self.val = val
        self.next = None
        self.prev = None


class DoublyLinkedList:
    def __init__(self):
        self.length = 0
        self.head = None
        self.tail = None

    def push(self, val):
        new_val = Node(val)

        if self.length == 0:
            self.head = new_val
            self.tail = new_val
        else:
            self.tail.next = new_val
            new_val.prev = self.tail
            self.tail = new_val

        self.length += 1
        return self

    def pop(self):
        # remove current tail
        if self.head == None:
            return None

        popped_Node = self.tail

        self.tail = popped_Node.prev
        self.tail.next = None
        popped_Node.prev = None
        #Need to set popped node prev to None as well, else users can still access previous links via removed.prev through the returned popped node

        self.length -= 1

        if self.length == 0:
            self.head = None
            self.tail = None
        return popped_Node

    def shift(self):
        if self.length == 0:
            return None

        old_head = self.head

        self.head = old_head.next
        self.head.prev = None
        old_head.prev = None

        self.length -= 1
        return old_head

    def unshift(self, val):
        new_node = Node(val)

        if self.length == 0:
            self.head = new_node
            self.tail = new_node
            return self

        new_node.next = self.head
        self.head.prev = new_node
        self.head = new_node
        self.length += 1

        return self

    def get(self, index):
        if self.length == 0:
            return None
        if index < 0 or index >= self.length:
            return False

        midpoint = self.length / 2

        if index > midpoint:
            curr = self.tail
            steps = self.length - 1

            while steps > index:
                curr = curr.prev
                steps -= 1
        else:
            curr = self.head
            steps = 0

            while steps < index:
                curr = curr.next
                steps += 1


        return curr

    def set(self, index, val):
        # Update a value in place
        # If index doesn't exist, just return false or error

        curr = self.get(index)
        if curr != False:
            curr.val = val
            return self
        return False

    def insert(self, index, val):

        if index < 0 or index > self.length: return False
        if index == 0: return self.unshift(val)
        if index == self.length: return self.push(val)

        prev_node = self.get(index-1)
        after_node = prev_node.next
        new_node = Node(val)

        prev_node.next = new_node
        new_node.prev = prev_node

        new_node.next = after_node
        after_node.prev = new_node

        self.length += 1

        return self

    def makeArray(self):
        array = []
        curr = self.head
        for i in range(self.length):
            array.append(curr.val)
            curr = curr.next
        return array

    def remove(self,index):
        if index < 0 or index >= self.length: return False
        if index == 0: return self.shift()
        if index == self.length - 1: self.pop()

        removed_node = self.get(index)
        prev_node = self.get(index-1)
        after_node = self.get(index+1)

        prev_node.next = after_node
        after_node.prev = prev_node

        removed_node.prev = None
        removed_node.next = None
        # These two steps are important. Otherwise connection between removed node and list exists. Users could access removed_node.prev or removed_node.next if these two commands aren't written

        self.length -= 1

        return removed_node




test = DoublyLinkedList()

for i in range(10):
    test.push(i+1)
