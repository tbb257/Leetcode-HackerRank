class Node():
    def __init__(self, val):
        self.val = val
        self.next = None



class SinglyLinkedList():
    def __init__(self):
        self.length = 0
        self.head = None
        self.tail = None

    def push(self, val):
        new_node = Node(val)

        if self.head == None:
            self.head = new_node
            self.tail = new_node
        else:
            self.tail.next = new_node
            self.tail = new_node

        self.length += 1
        return self

    def traverse(self):
        array = []
        curr = self.head
        while curr:
            array.append(curr.val)
            last = curr
            curr = curr.next
        print(array)
        return last.val

    def pop(self):
        if self.head == None:
            return None
        curr = self.head
        new_tail = self.head

        while curr.next:
            new_tail = curr
            curr = curr.next

        self.tail = new_tail
        self.tail.next = None
        self.length -= 1

        if self.length == 0:
            self.head = None
            self.tail = None

        return self.tail.val

    def shift(self):
        if self.head == None:
            return None

        shifted = self.head
        self.head = self.head.next
        self.length -= 1

        if self.length == 0:
            self.tail = None

        return shifted.val

    def unshift(self, val):
        new_node = Node(val)
        if self.head == None:
            self.head = new_node
            self.tail = new_node
            self.length = 1
        else:
            new_node.next = self.head
            self.head = new_node
        self.length += 1
        return self.head.val

    def get(self, index):
        if index < 0 or index >= self.length:
            return (f"Provided index is less than 0 or greather than list size. Current max index is {self.length - 1}.")

        curr = self.head
        steps = 0

        while steps < index:
            curr = curr.next
            steps += 1

        return curr

    def set(self, index, value):
        if self.head == None:
            new_Node = Node(value)
            self.head = new_Node
            self.tail = new_Node
            self.length = 1
            return self.head.val

        steps = 0
        curr = self.head
        while steps < index:
            curr = curr.next
            steps += 1
        curr.val = value
        return curr.val

    def insert(self, index, value):
        if self.length == 0:
            return self.unshift(value)
        if index > self.length or index < 0 :
            return "Error, use different index"
        if index == self.length:
            return self.push(value)

        new_node = Node(value)
        prev = self.get(index-1)

        temp = prev.next
        prev.next = new_node
        new_node.next = temp
        return new_node

    def remove(self, index):
        if index < 0 or index >= (self.length): return "Error"
        if index == 0: return self.shift
        if index == self.length-1: return self.pop()

        prev = self.get(index-1)
        removed = prev.next
        new_next = prev.next.next
        prev.next = new_next
        self.length -= 1
        return removed

    def reverse(self):
        curr = self.head

        self.head = self.tail
        self.tail = curr

        new_prev = None

        for i in range(self.length):
            snapshot = curr.next
            curr.next = new_prev
            new_prev = curr
            curr = snapshot
        return self.traverse()

test = SinglyLinkedList()
test.push(25)
test.push(30)
test.push(35)
test.push(40)
test.push(45)
test.push(50)
test.push(55)

test.traverse()
print(test.reverse())

