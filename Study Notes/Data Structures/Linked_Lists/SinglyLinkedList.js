class Node {
    constructor(val){
        this.val = val;
        this.next = null;
    }

}


class SinglyLinkedList{
    constructor(){
        this.length = 0;
        this.head = null;
        this.tail = null;
    }

    push(val){
        var newNode = new Node(val);
        if (this.head === null){
            this.head = newNode;
            this.tail = this.head;
            // Remember that the tail is just a pointer, pointing at last node. If only one node exists, head is same as tail
        }else{
            this.tail.next = newNode;
            // This updates the linked list, setting the new node as an extension of the tail, which is initially the last value

            this.tail = newNode;
            // This moves the tail (pointer) to the new node, which is now the last node. This makes code optimal, making every continuous addition look at initial tail, extending it, and updating tail
        }
        this.length ++;
        return this;
    }

    traverse(){
        var curr = this.head;
        // set pointer to head
        while (curr){
            console.log(curr.val);
            curr = curr.next;
            // traverses through list. Will fail on next 'while' check when curr = null
        }
    }

    pop(){
        if (!this.head) return undefined;
        var curr = this.head;
        var newTail = curr;

        while (curr.next){
            newTail = curr;
            curr = curr.next;
        }
        this.tail = newTail;
        this.tail.next = null;
        this.length--;
        if (this.length == 0){
            this.head = null;
            this.tail = null;
        }
        return curr.val
    }

    shift(){
        if (!this.head) return undefined;
        var newHead = this.head
        this.head = this.head.next
        this.length--
        if (this.length == 0){
            this.tail = null
        }
        return newHead.val
    }

    unshift(val){
        var new_node = new Node(val)
        if (!this.head){
            this.head = new_node
            this.head = new_node
        }
        else{
            new_node.next = this.head
            this.head = new_node
        }
        this.length += 1
        return this.head.val
    }

    get(index){
        if (index < 0 || index > this.length){
            return `Provided index is less than 0 or greater than list size. Current max allowable index is ${this.length - 1}.`
        }
        else{
            var curr = this.head;
            var steps = 0;
            while (steps < index){
                curr = curr.next;
                steps++;
            }
        }
        return curr;
    }

    set(index, value){
        if (!this.head){

            var newNode = new Node(value);
            this.head = newNode;
            this.tail = newNode;
            this.length ++;
            return this.head.val;

        }
        else{

            var curr = this.head;
            var steps = 0;

            while (steps < index){
                curr = curr.next;
                steps ++;
            }

            curr.val = value
        }
        return curr.val
    }

    insert(index, value){
        if (this.length === 0) return this.unshift(value);
        if (index > this.length || index < 0) return false;
        if (index === this.length) return this.push(value)

        var newNode = new Node(value)
        var prev = this.get(index-1)

        var temp = prev.next
        prev.next = newNode
        newNode.next = temp
        return newNode
    }

    remove(index){
        if (this.length === 0) return this.shift();
        if (index >= this.length || index < 0) return false;
        if (index === this.length-1) return this.pop(value)

        var prev = this.get(index-1)
        var removed = prev.next
        var newNext = prev.next.next
        prev.next = newNext
        this.length--
        return removed
    }

    reverse(){
        var node = this.head;
        this.head = this.tail;
        this.tail = node;

        var next;
        var prev = null;

        for(var i = 0; i < this.length; i++){
            next = node.next
            // snapshot of next value that we're going to move onto in the reversal process

            node.next = prev
            // first instance will make next = null, since initial head is now tail

            prev = node
            // update node to be current value so for next interation, you set it equal to be the 'next' value

            node = next
            // progress with the original snapshot
            return this
        }
    }
}


var list = new SinglyLinkedList()
list.push(1)
list.push(2)
list.push(3)
list.push(4)
list.push(5)
list.push(6)
list.push(7)
list.push(8)

console.log(list)
console.log(list.reverse())
