"""
Similar in structure to Trees -> Trees are a type of graph with limitations
Graphs don't have unique node known as root
Graphs are simply a collection of nodes with connections to one another

Trees cannot have siblings connected to one another through edges, Graphs have no limitation -> Nodes can connect to any node
Graphs can look like pure chaos

"""

class Graph:
    def __init__(self):
        self.adjacency_list = {}

    def add_vertex(self, key):
        if key in self.adjacency_list:
            print("Error, this vertex already exists.",{key:self.adjacency_list[key]})
            return
        self.adjacency_list[key] = []
        return self

    def add_edge(self, v1, v2):
        if (v1 not in self.adjacency_list) or (v2 not in self.adjacency_list):
            print("Error, one (or both) of your vertex don't exist within the graph yet.")
            return
        self.adjacency_list[v1].append(v2)
        self.adjacency_list[v2].append(v1)
        return

    def remove_edge(self, v1, v2):
        if (v1 not in self.adjacency_list[v2]) or (v2 not in self.adjacency_list[v1]):
            print("This edge does not currently exist.")
            return
        self.adjacency_list[v1].remove(v2)
        self.adjacency_list[v2].remove(v1)
        return self

    def remove_vertex(self, v1):
        if v1 not in self.adjacency_list:
            print ("Error, this vertex does not exist.")
            return
        while len(self.adjacency_list[v1]) != 0:
            self.remove_edge(v1, self.adjacency_list[v1][0])
        self.adjacency_list.pop(v1)
        return self


graph = Graph()
cities = ["New York", "Dallas", "Austin", "San Francisco", "Seattle"]
for city in cities:
    graph.add_vertex(city)


graph.add_edge("New York", "Dallas")
graph.add_edge("New York", "Austin")
graph.add_edge("Dallas", "San Francisco")
graph.add_edge("New York", "San Francisco")
graph.add_edge("New York", "Seattle")


graph.remove_vertex("New York")
graph.remove_vertex("New York")
