class Graph:
    def __init__(self):
        self.adjacency_list = {}

    def add_vertex(self, key):
        if key in self.adjacency_list:
            print("Error, this vertex already exists.",{key:self.adjacency_list[key]})
            return
        self.adjacency_list[key] = []
        return self

    def add_edge(self, v1, v2):
        if (v1 not in self.adjacency_list) or (v2 not in self.adjacency_list):
            print("Error, one (or both) of your vertex don't exist within the graph yet.")
            return
        self.adjacency_list[v1].append(v2)
        self.adjacency_list[v2].append(v1)
        return

    def remove_edge(self, v1, v2):
        if (v1 not in self.adjacency_list[v2]) or (v2 not in self.adjacency_list[v1]):
            print("This edge does not currently exist.")
            return
        self.adjacency_list[v1].remove(v2)
        self.adjacency_list[v2].remove(v1)
        return self

    def remove_vertex(self, v1):
        if v1 not in self.adjacency_list:
            print ("Error, this vertex does not exist.")
            return
        while len(self.adjacency_list[v1]) != 0:
            self.remove_edge(v1, self.adjacency_list[v1][0])
        self.adjacency_list.pop(v1)
        return self

    def DFS_Recursive(self, start):

        if start not in self.adjacency_list:
            print ("Error, this start vertex doesn't exist")
            return start

        result = []
        visited = {}

        def traverse(vertex):
            if not vertex:
                return

            visited[vertex] = True
            result.append(vertex)
            stack = self.adjacency_list[vertex]
            for neighbor in stack:
                if neighbor not in visited:
                    # We initially wrote return traverse(neighbor). Removed return once we realize this would stop each for-loop after first loop
                    # F would never get traversed
                    traverse(neighbor)

        traverse(start)
        return result

    def DFS_Iterative(self,start):

        if start not in self.adjacency_list:
            print ("Error, this start vertex doesn't exist")
            return start

        stack = []
        visited = {}
        result = []

        stack.append(start)

        while stack != []:
            vertex = stack.pop()

            if vertex not in visited:
                visited[vertex] = True
                result.append(vertex)
                for neighbor in self.adjacency_list[vertex]:
                    stack.append(neighbor)

        return result

graph = Graph()
points = ["A", "B", "C", "D", "E", "F"]
for char in points:
    graph.add_vertex(char)
graph.add_edge("A", "B")
graph.add_edge("A", "C")
graph.add_edge("B", "D")
graph.add_edge("C", "E")
graph.add_edge("D", "E")
graph.add_edge("D", "F")
graph.add_edge("E", "F")

print(graph.adjacency_list)

print(graph.DFS_Recursive("R"))
print(graph.DFS_Iterative("R"))
