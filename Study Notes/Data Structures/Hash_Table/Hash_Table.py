"""
Built in Dictionary acts as hash table
We're going to create a function
"""

def hash(key, arrayLen):
    hash_sum = 0
    for char in key:
        val = ord(char) - 96
        hash_sum = (hash_sum+val) % arrayLen
    return hash_sum


test = "hello"
print(hash(test,11))
