from Binary_Search_Trees import BinaryNode, BinarySearchTree

"""
DFS is implemented with a queue (first in. first out) Can also just use a regular array, and pop accordingly.
Post-Order starts bottom-up. Will find all sibling nodes before moving up a level.

                10
        6               15
    3       8                   20

"""

def DFS_Postorder(tree):
    visited = []
    curr = tree.root

    def traverse(node):
        """
        You only append node value once confirmed no left/right child node exists.
        Example; will traverse from 10->6->3. 3 has no children, gets added via line 26. Recursion on 6 left ends, so recursion on 6 right starts.
        8 has no children, so it gets added via line 26. Recursion on 6 left and right ends, so 6 gets added with line 26.
        Recursion on 10 left now ends, so 10 right starts. Rinse and repeat.
        """
        if node.left:traverse(node.left)
        if node.right:traverse(node.right)
        visited.append(node.value)

    traverse(curr)

    return visited


tree = BinarySearchTree()
values = [10,6,15,3,8,20]
for value in values:
    tree.insert(value)

print(DFS_Postorder(tree))
