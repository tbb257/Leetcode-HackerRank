from Binary_Search_Trees import BinaryNode, BinarySearchTree

"""
DFS is implemented with a queue (first in. first out) Can also just use a regular array, and pop accordingly.
In-Order starts bottom-up. Will start at left-most child, traverse up to parent, check if it has a sibling node, and then traverse up again.
Visual idea is to start bottom-right, traverse entire left, traverse to parent root and then traverse right.

                10
        6               15
    3       8                   20

"""

def DFS_InOrder(tree):
    visited = []
    curr = tree.root

    def traverse(node):
        """
        You only append node value once confirmed no left child node exists.
        Example; will traverse from 10->6->3. 3 has no left child, gets added. Recursion on 6.left ends, so 6 gets added with line 18. Then we check right child
        Right child is 8. Recurse on node 8, there is no node.left, so we add 8. Then recursion on 10.left ends, so we add 10. Then check right child, and repeat
        """
        if node.left: traverse(node.left)
        visited.append(node.value)
        if node.right: traverse(node.right)

    traverse(curr)

    return visited


tree = BinarySearchTree()
values = [10,6,15,3,8,20]
for value in values:
    tree.insert(value)

print(DFS_InOrder(tree))
