
class BinaryNode:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None

class BinarySearchTree:
    def __init__(self):
        self.root = None

    def insert(self, value):

        new_node = BinaryNode(value)

        if self.root == None:
            self.root = new_node
            return self

        curr = self.root
        while curr:
            if new_node.value < curr.value:
                if curr.left == None:
                    curr.left = new_node
                    return self
                else:
                    curr = curr.left
            elif new_node.value > curr.value:
                if curr.right == None:
                    curr.right = new_node
                    return self
                curr = curr.right
            else:
                print("Node value already exists")
                return None

        return self


    def find(self, value):

        if self.root is None:
            return False

        curr = self.root

        while curr:
            if value < curr.value:
                if curr.left == None:
                    return False
                else: curr = curr.left
            elif value > curr.value:
                if curr.right == None:
                    return False
                else: curr = curr.right
            else: return True
        return False

    # def insert(self, root, value):
        # if root is None:
        #     root = BinaryNode(value)

        # else:
        #     if value < root.value:
        #         root.left = self.insert(root.left, value)
        #     elif value > root.value:
        #         root.right = self.insert(root.right, value)
        #     else:
        #         print ("Node value already exists")
        #         return None

        # return root

    # def find(self, root, value):
        # if root is None:
        #     return False
        # if root.value == value:
        #     return True
        # else:
        #     if value < root.value:
        #         return self.find(root.left, value)
        #     else:
        #         return self.find(root.right, value)


# tree = BinarySearchTree()
# tree.root = BinaryNode(10)
# tree.root.left = BinaryNode(5)
# tree.root.right = BinaryNode(15)

# tree.insert(9)
# tree.insert(25)

# tree.insert(tree.root, 9)
# tree.insert(tree.root, 25)

# print(tree.find(6))
# print(tree.find(5))
# print(tree.find(9))
# print(tree.find(15))

# print(tree.find(tree.root, 6))
# print(tree.find(tree.root, 5))
# print(tree.find(tree.root, 9))
# print(tree.find(tree.root, 15))
