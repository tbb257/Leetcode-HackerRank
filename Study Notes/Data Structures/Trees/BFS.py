from Binary_Search_Trees import BinaryNode, BinarySearchTree

"""
BFS is implemented with a queue (first in. first out) Can also just use a regular array, and pop accordingly.
BFS is top-down. Checks all child nodes of current node, before progressing to each child's own children, iteratively.
Imagine going down one level of a tree, then going across that level entirely, before progressing down to next level.

                10
        6               15
    3       8                   20

"""

def BFS_traverse(tree):
    queue = [tree.root]
    visited = []

    while queue:
        curr = queue.pop(0)
        visited.append(curr.value)

        if curr.left is not None:
            queue.append(curr.left)
        if curr.right is not None:
            queue.append(curr.right)

    return visited


tree = BinarySearchTree()

values = [10,6,15,3,8,20]

for value in values:
    tree.insert(value)

print(BFS_traverse(tree))
