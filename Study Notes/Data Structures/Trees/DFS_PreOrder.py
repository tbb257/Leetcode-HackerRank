from Binary_Search_Trees import BinaryNode, BinarySearchTree

"""
DFS is implemented with a queue (first in. first out) Can also just use a regular array, and pop accordingly.
DFS is also top-down.
Imagine going down to final level vertically off one edge, then moving up a level once just to check if the previous level had a sibling node, iteratively.
Will only move up a level again once no sibling nodes are found. If sibling nodes are found, go all the way down that edge again.

                10
        6               15
    3       8                   20

"""

def DFS_Preorder(tree):
    visited = []
    curr = tree.root

    def traverse(node):
        """
        You append value first before traversing, since you traverse all the way down left. After append, you recursively keep checking left.
        You only check right once left recursion on any node fails.
        Example: 10 gets added, recurse on 10 left ->6. 6 gets added, recurse on 6 left -> 3. 3 gets added but, but no left and right.
        Recursion on 6 left ends, so now we finally check 6 right -> 8. 8 gets added, no children, so recursion on 6 right ends.
        Recursion on 10 left now complete, so we finally check 10 right.
        """
        visited.append(node.value)
        if node.left: traverse(node.left)
        if node.right: traverse(node.right)

    traverse(curr)

    return visited

def DFS_Preorder_Iteratively(tree):
    stack = [tree.root]
    visited = []

    while stack:
        curr = stack.pop()
        visited.append(curr.value)

        # Append curr.left last, because you want next traversal to start with curr.left, so it needs to be last for stack to pop correctly
        if curr.right is not None:
            stack.append(curr.right)
        if curr.left is not None:
            stack.append(curr.left)

    return visited



tree = BinarySearchTree()
values = [10,6,15,3,8,20]
for value in values:
    tree.insert(value)

print(DFS_Preorder(tree))
