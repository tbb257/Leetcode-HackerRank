class Node:
    def __init__(self, val):
        self.val = val
        self.next = None


class Stack:
    def __init__(self):
        self.first = None
        self.last = None
        self.size = 0

    def push(self, val):
        new_node = Node(val)

        if self.size == 0:
            self.first = new_node
            self.last = new_node
        else:
            temp = self.first
            # stack push should always push new values to the front, aka first. Store curr first as temp variable to set equal to new val's 'next', connecting everything
            self.first = new_node
            self.first.next = temp

        self.size += 1
        return self

    def pop(self):
        if self.size == 0:
            return None

        removed = self.first

        if self.first == self.last:
            self.last = None

        self.first = self.first.next
        removed.next = None
        self.size -= 1

        return removed


class Queue:
    def __init__(self):
        self.first = None
        self.last = None
        self.size = 0

    def enqueue(self,val):
        new_node = Node(val)

        if self.size == 0:
            self.first = new_node
            self.last = new_node

        self.last.next = new_node
        self.last = new_node

        self.size += 1

        return self

    def dequeue(self):
        if self.size == 0:
            return None

        removed = self.first
        self.first = self.first.next
        removed.next = None

        if self.first == self.last:
            self.last = None

        self.size -= 1

        return removed


test = Queue()

for i in range(1,5):
    value = i*5
    test.enqueue(value)

test.dequeue()
test.dequeue()
test.dequeue()

print(test.first.val)
