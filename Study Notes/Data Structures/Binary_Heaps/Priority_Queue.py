class Node:
    def __init__(self, val, priority):
        self.val = val
        self.priority = priority

class PriorityQueue:
    def __init__(self):
        self.values = []

    def enqueue(self, val, priority):
        node = Node(val, priority)
        self.values.append(node)

        if len(self.values) > 1:
            self.bubbleup()

        return self

    def bubbleup(self):
        idx = len(self.values) - 1
        element = self.values[idx]

        while idx > 0:
            parent_idx = (idx-1) // 2
            parent = self.values[parent_idx]

            if (element.priority) <= (parent.priority):
                break
            else:
                self.values[parent_idx] = element
                self.values[idx] = parent

                idx = parent_idx
        return self

    def dequeue(self):
        max = self.values[0]
        end = self.values.pop()
        if len(self.values) > 0:
            self.values[0] = end
            self.sinkdown()
        return max

    def sinkdown(self):
        idx = 0
        length = len(self.values)
        parent = self.values[idx]
        while True:
            idx_left, idx_right = (2*idx+1),(2*idx+2)
            swap = False

            if idx_left < length:
                left_child = self.values[idx_left]
                if left_child.priority > parent.priority:
                    swap = idx_left
            if idx_right < length:
                right_child = self.values[idx_right]
                if (swap == False and right_child.priority > parent.priority) or (swap != False and right_child.priority > left_child.priority):
                    swap = idx_right

            if swap == False:
                break

            self.values[idx], self.values[swap] = self.values[swap], self.values[idx]

            idx = swap
        return self



ER = PriorityQueue()
ER.enqueue("Common Cold", 1)
ER.enqueue("Gunshot wound", 5)
ER.enqueue("High Fever", 2)

print(ER.dequeue().val)
print(ER.dequeue().val)
print(ER.dequeue().val)
print(ER.values)
