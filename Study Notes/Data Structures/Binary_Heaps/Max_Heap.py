"""
Heaps are type of trees, with slightly different rules on node distribution
MaxHeap - Value of parent node ALWAYS greater than its children. Example
                33
        25              20
    12      5        10      6

No pattern between sibling nodes.
Must be compact as possible - both left and right child must be filled before progressing
Left children filled out first

"""

class MaxBinaryHeap:
    def __init__(self):
        self.heap = []

    def insert(self, value):
        # Simply insert new value into end, call helper function to rearrange
        self.heap.append(value)

        if len(self.heap) == 1:
            return self

        self.bubble_up()
        return self

    def bubble_up(self):

        """
        For each parent index, childs located at (2n-1) and (2n-2)
        Likewise when inserting, to find right location, child = last index
        Calculate parent as (child-1) //2
        If child is greater than parent, they swap. Update index of child to be parent index, and compare to new parent index
        Iterate until child is finally less than its parent, or its index is no longer greater than 0
        If child index becomes 0, this means it's the greatest value in the entire heap

        """
        child = len(self.heap) - 1
        c_element = self.heap[child]

        while child > 0:
            parent = (child-1) // 2
            p_element = self.heap[parent]

            if c_element > p_element:
                self.heap[parent] = c_element
                self.heap[child] = p_element

                child = parent
            else:
                break

        return self

    def extractMax(self):
        # Convention is to remove last value in array, and set root value equal to it
        # Call helper function to rearrange
        if len(self.heap) == 0:
            return None

        max = self.heap[0]
        end = self.heap.pop()

        if len(self.heap) != 0:
            self.heap[0] = end
            self.sink_down()

        return max

    def sink_down(self):
        """
        Start at parent, and look at its children iteratively. Swap parent with child if child is larger
        If both children are larger, swap with larger value. Convention is to create left/right index and element
        But declare a swap value, which is the final value to be swapped with parent if conditions met.

        Only time you should consider right child is if swap condition is kept at False (meaning left child < parent) or if swap is not False, but right is greater than left
        Set condition to only even consider both left and right if they're less than length of heap (so not out of bounds)

        Update parent index to be equal to swapped index, to trickle down. Repeat
        Only break if swap remains false even after considering left and right children. Implies parent is now greater than both children
        """
        idx = 0
        p_element = self.heap[0]
        length = len(self.heap)

        while True:
            idx_leftChild, idx_rightChild = (2*idx)+1, (2*idx)+2
            swap = False

            if idx_leftChild < length:
                leftChild = self.heap[idx_leftChild]
                if leftChild > p_element:
                    swap = idx_leftChild
            if idx_rightChild < length:
                rightChild = self.heap[idx_rightChild]
                if (swap == False and rightChild > p_element) or (swap != False and rightChild > leftChild):
                    swap = idx_rightChild

            if swap == False:
                break

            self.heap[idx], self.heap[swap] = self.heap[swap], self.heap[idx]
            # self.heap[swap] = p_element
            idx = swap

        return self

test = MaxBinaryHeap()

values = [41,39,33,18,27,12,55]
for value in values:
    test.insert(value)


print(test.heap)
print(test.extractMax())


print(test.heap)
